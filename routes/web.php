<?php

Route::post('markers', 'MarkersController@store');
Route::get('markers', 'MarkersController@show');
Route::delete('markers/{marker}', 'MarkersController@delete')->middleware('bindings');

Route::get('anchors/{anchor}/markers', 'MarkersController@index')->middleware('bindings');
