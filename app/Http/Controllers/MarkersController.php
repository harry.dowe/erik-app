<?php

namespace App\Http\Controllers;

use App\Http\Requests\MarkerRequest;
use App\Models\Marker;
use Exception;
use Illuminate\Http\JsonResponse;

class MarkersController extends Controller
{
    /**
     * @param MarkerRequest $request
     *
     * @return JsonResponse
     */
    public function store(MarkerRequest $request): JsonResponse
    {
        Marker::create($request->json()->all());

        return new JsonResponse([
            'success' => true,
        ]);
    }

    /**
     * @param $anchor
     *
     * @return JsonResponse
     */
    public function index($anchor): JsonResponse
    {
        $markers = Marker::where('anchor_id', $anchor)->get();

        return new JsonResponse($markers);
    }

    /**
     * @param Marker $marker
     *
     * @return JsonResponse
     */
    public function delete(Marker $marker): JsonResponse
    {
        try {
            $success = $marker->delete();
        } catch (Exception $exception) {
            $success = false;
        }

        return new JsonResponse([
            'success' => $success === true,
        ]);
    }
}
