<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Marker extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'anchor_id',
        'marker_type_id',
        'x_coordinate',
        'y_coordinate',
        'z_coordinate',
    ];

}
